'use strict';

const moment = require('moment')

module.exports = () => {
    return {
        start: moment().subtract(1, 'month').toISOString(),
        end: moment().subtract(1, 'week').toISOString()
    }
};
