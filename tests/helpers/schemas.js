'use strict'

const joi = require('joi')

const temperatures = joi.array().items(
    joi.object({
        temp: joi.number().required(),
        date: joi.date().iso().required()
    })
)

const winds = joi.array().items(
    joi.object({
        north: joi.number().required(),
        west: joi.number().required(),
        date: joi.date().iso().required()
    })
)

const weather = joi.array().items(
    joi.object({
        temp: joi.number().required(),
        north: joi.number().required(),
        west: joi.number().required(),
        date: joi.date().iso().required()
    })
)

module.exports = {
    temperatures,
    winds,
    weather
}
