'use strict';

const server = require('server')
const request = require('supertest')(server)
const httpStatus = require('http-status')
const joi = require('joi')
const querystring = require('qs');
const moment = require('moment')

const schemas = require('tests/helpers/schemas')
const interval = require('tests/helpers/payload')

describe('GET /weather', () => {
    it('should successfully return weather', (done) => {
        const query = querystring.stringify(interval());

        request.get(`/weather?${query}`)
            .expect('Content-type', /json/)
            .expect(httpStatus.OK)
            .expect((res) => {
                const schema = schemas.weather

                joi.assert(res.body, schema)
            })
            .end(done)
    });

    ['start', 'end'].forEach(key => {
        it(`should fail to return weather if ${key} is missing`, (done) => {
            const payload = interval()
            delete payload[key]

            const query = querystring.stringify(payload);

            request.get(`/weather?${query}`)
                .expect(httpStatus.BAD_REQUEST)
                .end(done)
        })
    })

    it(`should fail to return weather if start is after end`, (done) => {
        const payload = interval()
        payload.end = moment(payload.end).subtract(1, 'month').toISOString()

        const query = querystring.stringify(payload);

        request.get(`/weather?${query}`)
            .expect(httpStatus.BAD_REQUEST)
            .end(done)
    })
})
