# Use the predefined node base image for this module.
FROM node:15.4.0

# Fix for timezone. Set the timezone in the image to the desired application timezone
RUN ln -fs /usr/share/zoneinfo/Europe/Berlin /etc/localtime && dpkg-reconfigure -f noninteractive tzdata

# create the log directory
RUN mkdir -p /var/log/applications/weather_service

# Creating base "src" directory where the source repo will reside in our container.
# Code is copied from the host machine to this "src" folder in the container as a last step.
RUN mkdir /src
WORKDIR /src
COPY . /src

# Install node dependencies
RUN npm install
RUN npm install pm2 -g

# Map a volume for the log files
VOLUME ["/var/log/applications/weather_service"]

ADD docker/scripts/start.sh /start.sh
RUN chmod +x /start.sh
CMD /start.sh
