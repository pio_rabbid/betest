'use strict';

const express = require('express');

const config = require('app/config/config');
const serviceLocator = require('app/config/di');
const router = require('app/routes/routes')(serviceLocator);

const logger = serviceLocator.get('logger');
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use('/', router);

const server = app.listen(config.web_server.port, () => {
    logger.info(`Server running on port: ${config.web_server.port}`);
});

// For regression tests purpose
module.exports = server;
