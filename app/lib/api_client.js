'use strict';

const querystring = require('qs');
const request = require('requestretry').defaults({ json: true, maxAttempts: 2 });

class Request {
    constructor(baseUrl, port) {
        this.url = baseUrl;
        this.port = port;
    }

    get(path, params) {
        params = params || '';
        const query = querystring.stringify(params);
        const reqUrl = `http://${this.url}:${this.port}${path}?${query}`;

        return request.get(reqUrl).then((response) => {
            if (response.statusCode >= 400) {
                throw new Error('API Server error');
            }

            return response.body;
        }).catch((err) => {
            throw err;
        });
    }
}

module.exports = Request;
