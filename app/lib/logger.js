'use strict';

// just imagine a proper custom logging library here
module.exports = () => {
    return {
        info: (message) => {
            console.info(message);
        },
        debug: (message) => {
            console.debug(message);
        },
        warning: (message) => {
            console.warn(message);
        },
        error: (message, error) => {
            console.error(message);
        },
        fatal: (message, error) => {
            console.error(message);
        }
    }
};
