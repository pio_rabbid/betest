'use strict';

const joi = require('joi');

module.exports = joi.object().keys({
    start: joi.date().iso().required(),
    end: joi.date().iso().min(joi.ref('start')).required()
});
