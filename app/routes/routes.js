'use strict';

const router = require('express').Router();
const validator = require('express-joi-validation').createValidator({});

const schemas = require('app/routes/validation_schemas/index');

module.exports = (serviceLocator) => {
    const weatherController = serviceLocator.get('weatherController');

    router.get(
        '/temperatures',
        validator.query(schemas.dates),
        (req, res) => weatherController.getTemperatures(req, res)
    );

    router.get(
        '/speeds',
        validator.query(schemas.dates),
        (req, res) => weatherController.getWindSpeeds(req, res)
    );

    router.get(
        '/weather',
        validator.query(schemas.dates),
        (req, res) => weatherController.getWeather(req, res)
    );

    return router;
};
