'use strict';

const httpStatus = require('http-status');
const errors = require('app/lib/errors');

class WeatherController {
    /**
     * @constructor
     *
     * @param {weatherService} weatherService
     * @param {logger} logger
     */
    constructor(weatherService, logger) {
        this.weatherService = weatherService;
        this.logger = logger;
    }

    /**
     * Get temperatures endpoint: handles GET /temperatures
     *
     * @method getTemperatures
     * @param req
     * @param res
     * @returns {*}
     */
    getTemperatures(req, res) {
        const query = req.query;

        return this.weatherService.getTemperatures(query)
            .then((resp) => res.send(resp))
            .catch((error) => {
                switch (error.constructor) {
                    case errors.InvalidParams:
                        error.code = httpStatus.BAD_REQUEST;
                        break;
                    default:
                        error.code = httpStatus.INTERNAL_SERVER_ERROR;
                        error = errors.InternalServerError('Internal Server Error');
                }

                this.logger.error(`Failed to create records. ${error}`);
                res.send(error);
            });
    }

    /**
     * Get wind speeds endpoint: handles GET /speeds
     *
     * @method getWindSpeeds
     * @param req
     * @param res
     * @returns {*}
     */
    getWindSpeeds(req, res) {
        const query = req.query;

        return this.weatherService.getWindSpeeds(query)
            .then((resp) => res.send(resp))
            .catch((error) => {
                switch (error.constructor) {
                    case errors.InvalidParams:
                        error.code = httpStatus.BAD_REQUEST;
                        break;
                    default:
                        error.code = httpStatus.INTERNAL_SERVER_ERROR;
                        error = errors.InternalServerError('Internal Server Error');
                }

                this.logger.error(`Failed to translate records. ${error}`);
                res.send(error);
            });
    }

    /**
     * Get weather endpoint: handles GET /weather
     *
     * @method getWeather
     * @param req
     * @param res
     * @returns {*}
     */
    getWeather(req, res) {
        const query = req.query;

        return this.weatherService.getWeather(query)
            .then((resp) => res.send(resp))
            .catch((error) => {
                switch (error.constructor) {
                    case errors.InvalidParams:
                        error.code = httpStatus.BAD_REQUEST;
                        break;
                    default:
                        error.code = httpStatus.INTERNAL_SERVER_ERROR;
                        error = errors.InternalServerError('Internal Server Error');
                }

                this.logger.error(`Failed to translate records. ${error}`);
                res.send(error);
            });
    }
}

module.exports = WeatherController;
