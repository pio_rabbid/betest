'use strict';

const serviceLocator = require('app/lib/servicelocator');
const logger = require('app/lib/logger');

const weatherController = require('app/controllers/weather');
const weatherService = require('app/services/weather');

serviceLocator.register('logger', () => {
    return logger();
});

serviceLocator.register('weatherService', (locator) => {
    const logger = locator.get('logger');
    return new weatherService(logger);
});

serviceLocator.register('weatherController', (locator) => {
    const weatherService = locator.get('weatherService');
    const logger = locator.get('logger');
    return new weatherController(weatherService, logger);
});

module.exports = serviceLocator;
