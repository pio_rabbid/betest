'use strict';

const appName = 'weather_service';

const config = {
    appName: appName,
    web_server: {
        port: process.env.NODE_PORT || 8090
    },
    services: {
        wind_speed: {
            url: process.env.WIND_SPEED_URL || 'host.docker.internal',
            port: 8080
        },
        temperature: {
            url: process.env.TEMPERATURE_URL || 'host.docker.internal',
            port: 8000
        }
    }
};

module.exports = config;
