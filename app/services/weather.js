'use strict';

const moment = require('moment');

const config = require('app/config/config');
const Request = require('app/lib/api_client');

const windService = new Request(config.services.wind_speed.url, config.services.wind_speed.port);
const temperatureService = new Request(config.services.temperature.url, config.services.temperature.port);

class WeatherService {
    /**
     * @constructor
     *
     * @param {logger} logger
     */
    constructor(logger) {
        this.logger = logger;
    }

    /**
     * @method getTemperatures
     *
     * @param query {{}}
     *
     * @returns {Promise<>}
     */
    getTemperatures(query) {
        const requests = []
        const start = moment(query.start)
        const end = moment(query.end)

        while(start.isSameOrBefore(end)) {
            const date = start.toISOString()

            requests.push(temperatureService.get('/', { at: date }))
            start.add(1, 'day')
        }

        return this.getInChunks(requests).then(temperatures => {
            return temperatures.sort((a, b) => moment(a.date).diff(b))
        }).catch(error => {
            this.logger.error(`Failed to get temperatures. Error: ${error.message}`)
            throw error
        })
    }

    /**
     * @method getWindSpeeds
     *
     * @param query {{}}
     *
     * @returns {Promise<{}>}
     */
    getWindSpeeds(query) {
        const requests = []
        const start = moment(query.start)
        const end = moment(query.end)

        while(start.isSameOrBefore(end)) {
            const date = start.toISOString()

            requests.push(windService.get('/', { at: date }))
            start.add(1, 'day')
        }

        return this.getInChunks(requests).then(winds => {
            return winds.sort((a, b) => moment(a.date).diff(b))
        }).catch(error => {
            this.logger.error(`Failed to get winds. Error: ${error.message}`)
            throw error
        })
    }

    /**
     * @method getWeather
     *
     * @param query {{}}
     *
     * @returns {Promise<{}>}
     */
    getWeather(query) {
        return Promise.all([this.getTemperatures(query), this.getWindSpeeds(query)]).then(resp => {
            const temps = resp[0]
            const winds = resp[1]

            return winds.map((elem, idx) => {
                elem.temp = temps[idx].temp
                return elem
            })
        })
    }

    /**
     * @method getInChunks
     *
     * @param requests {[Request]}
     * @param responses {[{}]}
     *
     * @returns {Promise<[]>}
     */
    getInChunks(requests, responses = []) {
        const chunkSize = 10
        const part = requests.splice(0, chunkSize)

        return Promise.all(part).then(resp => {
            responses.push(...resp)

            if (requests.length > 0) {
                return this.getInChunks(requests, responses)
            }

            return responses
        }).catch(error => {
            this.logger.error(`Failed to get requests in chunks. Error: ${error.message}`)
            throw error
        })
    }
}

module.exports = WeatherService;
